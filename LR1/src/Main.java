import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        if(args[0].equals("Matrix")) {
            final int MATRIX_SIZE = 4;
            Matrix matrix = new Matrix(MATRIX_SIZE, "fileForMatrix");
            for (int i = 0; i < MATRIX_SIZE; i++) {
                if (matrix.checkToPositiveString(matrix.getLine(i))) {
                    System.out.println("String number: " + i);
                    System.out.println("Product of elements: " + matrix.productOfStringElements(matrix.getLine(i)) + "\n");
                }
            }
        }
        if(args[0].equals("String")){
            SomeString string = new SomeString("fileForString");
            string.changeString();
            System.out.println(string.getString());
        }
        if(args[0].equals("Text")){
            Text text = new Text("fileForText");
            for(var i: text.getWordsStartingByVowelLetter()){
                System.out.println(i);
            }
        }
    }
}