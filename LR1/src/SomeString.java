import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class SomeString {
    public SomeString(String filename) {
        Scanner scanner = null;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            string = reader.readLine();
            reader.close();
        } catch (InputMismatchException e) {
            throw new RuntimeException("There isn't string");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void changeString(){
        string = string.replace(".", "...");
    }

    public String getString(){
        return string;
    }

    private String string;
}