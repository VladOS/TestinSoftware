import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Matrix {
    public Matrix(int _size, String filename) throws FileNotFoundException {
        size = _size;
        matrix = new int[size][size];
        fillMatrix(filename);
    }

    public void fillMatrix(String filename) throws FileNotFoundException {
        Scanner scanner = null;
        try {
            File file = new File(filename);
            scanner = new Scanner(file);
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    matrix[i][j] = scanner.nextInt();
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Not founded file");
        } catch (InputMismatchException e) {
            assert scanner != null;
            scanner.close();
            throw new RuntimeException("There aren't enough numbers");
        }
    }

    public int[] getLine(int number) {
        int[] line = new int[size];
        System.arraycopy(matrix[number], 0, line, 0, size);
        return line;
    }

    public boolean checkToPositiveString(int[] line) {
        for (int i : line) {
            if (i < 0) return false;
        }
        return true;
    }

    public int productOfStringElements(int[] line) {
        int product = 1;
        for (int i : line) {
            if (i == 0) return 0;
            product *= i;
        }
        return product;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    private final int size;
    private final int[][] matrix;
}