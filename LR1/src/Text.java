import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Text {
    public Text(String filename) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String line;
            int wordsStartingByVowelLetterCount = 0;
            while ((line = reader.readLine()) != null) {
                String[] words = line.split(" ");

                for (String word : words) {
                    if (word.matches("(?i)^[aeiouy].*")) {
                        wordsStartingByVowelLetterCount += 1;
                    }
                }
            }
            wordsStartingByVowelLetter = new String[wordsStartingByVowelLetterCount];
            wordsStartingByVowelLetterCount = 0;
            reader = new BufferedReader(new FileReader(filename));
            while ((line = reader.readLine()) != null) {
                String[] words = line.split(" ");

                for (String word : words) {
                    if (word.matches("(?i)^[aeiouy].*")) {
                        wordsStartingByVowelLetter[wordsStartingByVowelLetterCount] = word;
                        wordsStartingByVowelLetterCount++;
                    }
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String[] getWordsStartingByVowelLetter() {
        return wordsStartingByVowelLetter;
    }

    private String[] wordsStartingByVowelLetter;
}